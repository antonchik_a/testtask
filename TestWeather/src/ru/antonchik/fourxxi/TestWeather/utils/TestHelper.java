package ru.antonchik.fourxxi.TestWeather.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

/**
 * Created by alex on 10.07.14.
 */
public class TestHelper {
    public static String LOG_TAG = "myLogs";



    public static boolean isNetworkConnected(Context _context)  //проверка подключени як интернету
    {
        ConnectivityManager cm = (ConnectivityManager) _context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        if (ni == null) {
            return false;
        } else
            return true;
    }

    public static void showToast(Context context,String text)
    {
        Toast.makeText(context.getApplicationContext(), text, Toast.LENGTH_LONG).show();
    }
}
