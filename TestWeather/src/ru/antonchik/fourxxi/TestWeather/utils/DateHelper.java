package ru.antonchik.fourxxi.TestWeather.utils;

import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by alex on 11.07.14.
 */
public class DateHelper {
    private static SimpleDateFormat sDateFormat = new SimpleDateFormat("dd.MM.yyyy hh:mm");

    public static Date getDateFromDotNetString(String date) {
        date = date.replace("/Date(", "").replace(")/", "");
        long time = Long.parseLong(date);
        Date result = new Date(time);
        int offset = result.getTimezoneOffset();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(result);
        offset = offset/60;
        calendar.add(Calendar.HOUR_OF_DAY,offset);
        return calendar.getTime();
    }

    public static String getDateMonthString(Date date) {
        String result = "";
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        result += calendar.get(Calendar.DAY_OF_MONTH) + " " + getMonth(calendar.get(Calendar.MONTH));

        return result;
    }

    public static String getDateMonthYearString(Date date) {
        String result = "";
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        result += calendar.get(Calendar.DAY_OF_MONTH) + " " + getMonth(calendar.get(Calendar.MONTH))+ " " + calendar.get(Calendar.YEAR);

        return result;
    }

    public static String getDateMonthStringWithTime(Date date) {
        String result = "";
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        result += calendar.get(Calendar.DAY_OF_MONTH) + " " + getMonth(calendar.get(Calendar.MONTH)) + " " + getTime(date);

        return result;
    }

    public static String getMonth(int month) {
        return new DateFormatSymbols().getMonths()[month];
    }


    public static Date DataTime(String dat) {
        String dtStart = dat;
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        try {
            Date date = format.parse(dat);
            System.out.println(date);
            return date;
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();

            return null;
        }
    }

    public static String DataTimeString(Date dat) {

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(dat);
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        try {

            return format.format(dat);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();

            return "";
        }
    }

    public static String getLastDate() {

        Calendar calendar = Calendar.getInstance();

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        try {

            return format.format(calendar.getTime());
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();

            return "";
        }
    }




    public static String getTime(Date dat) {

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(dat);
        SimpleDateFormat format = new SimpleDateFormat("HH:mm");
        try {

            return format.format(dat);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();

            return "";
        }
    }
}
