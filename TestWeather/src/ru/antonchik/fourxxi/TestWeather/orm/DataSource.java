package ru.antonchik.fourxxi.TestWeather.orm;

import ru.antonchik.fourxxi.TestWeather.models.City;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by alex on 12.03.14.
 */
public class DataSource
{

    public static void clearData() {
        try {
            DBManager.getInstance().getHelper().clearTables();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static ArrayList<City> getCities() {
        List<City> users = null;
        try {
            users = DBManager.getInstance().getHelper().getCityDao().queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if(users != null)
            return  new ArrayList<City>(users);
        else
            return  new ArrayList<City>();
    }

    public static City getActiveCity()
    {
        Map<String, Object> fieldValues = new HashMap<String, Object>();
        fieldValues.put("active", true);

        List<City> city = new ArrayList<City>();
        try {
            city = DBManager.getInstance().getHelper().getCityDao().queryForFieldValues(fieldValues);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        if(city.size() > 0)
        return city.get(0);
        else
            return null;
    }

    public static City getCityByName(String name)
    {
        Map<String, Object> fieldValues = new HashMap<String, Object>();
        fieldValues.put("cityName", name);

        List<City> city = new ArrayList<City>();
        try {
            city = DBManager.getInstance().getHelper().getCityDao().queryForFieldValues(fieldValues);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        if(city.size() > 0)
            return city.get(0);
        else
            return null;
    }

    public static void setActiveCity(City active)
    {
        Map<String, Object> fieldValues = new HashMap<String, Object>();
        fieldValues.put("active", true);

        List<City> cities = new ArrayList<City>();
        try {
            cities = DBManager.getInstance().getHelper().getCityDao().queryForFieldValues(fieldValues);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        if(cities.size() > 0)
        {
           for(City activeCity :cities)
           {
               activeCity.setActive(false);
               City.updateCity(activeCity);
           }
            active.setActive(true);
            City.updateCity(active);

        }

    }
}
