package ru.antonchik.fourxxi.TestWeather.models;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import ru.antonchik.fourxxi.TestWeather.orm.DBManager;
import ru.antonchik.fourxxi.TestWeather.owm.Weather;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by Alexey Antonchik on 07.07.14.
 */
@DatabaseTable(tableName = "city")
public class City implements Serializable {

    @DatabaseField(id = true)
    private Integer id;

    @DatabaseField
    private String cityID;

    @DatabaseField
    private String cityName;

    @DatabaseField
    private boolean active;

    @DatabaseField
    private double lat;

    @DatabaseField
    private double lon;


    @DatabaseField
    public String jsonStatus;

    @DatabaseField
    public String jsonForecastThreeDay;

    @DatabaseField
    public String jsonForecastWeek;


    private ArrayList<Weather> threeDayForecast;

    private ArrayList<Weather> weekForecast;

    private Weather dayWeather;

    public City( String cityID, String cityName, boolean active, double lat, double lon) {
        this.cityID = cityID;
        this.cityName = cityName;
        this.active = active;
        this.lat = lat;
        this.lon = lon;
    }

    public City() {
    }

    public void weatherAdjust()
    {
        if(jsonStatus != null)
            dayWeather = Weather.ajustStatus(jsonStatus,this);

        if(jsonForecastThreeDay != null)
            threeDayForecast = Weather.ajustForecast(jsonForecastThreeDay);

        if(jsonForecastWeek != null)
            weekForecast = Weather.ajustForecast(jsonForecastWeek);

    }

    public static void createInitialData()
    {
        City moscow = new City("524901","Москва",true,55.757,37.615);
        City st_petersburg = new City("498817","Санкт-петербург",false,30.306,59.9332);
        newCity(moscow);
        newCity(st_petersburg);
    }

    public static void newCity(City newCity) {
        try {
            DBManager.getInstance().getHelper().getCityDao().create(newCity);
        } catch (SQLException ex) {

        }
    }

    public static void updateCity(City updateCity) {
        try {
            DBManager.getInstance().getHelper().getCityDao().update(updateCity);
        } catch (SQLException ex) {

        }
    }

    public static void deleteCity(City deleteCity) {
        try {
            DBManager.getInstance().getHelper().getCityDao().delete(deleteCity);
        } catch (SQLException ex) {

        }
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCityID() {
        return cityID;
    }

    public void setCityID(String cityID) {
        this.cityID = cityID;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public String getJsonStatus() {
        return jsonStatus;
    }

    public void setJsonStatus(String jsonStatus) {
        this.jsonStatus = jsonStatus;
    }

    public ArrayList<Weather> getThreeDayForecast() {
        return threeDayForecast;
    }

    public void setThreeDayForecast(ArrayList<Weather> threeDayForecast) {
        this.threeDayForecast = threeDayForecast;
    }

    public ArrayList<Weather> getWeekForecast() {
        return weekForecast;
    }

    public void setWeekForecast(ArrayList<Weather> weekForecast) {
        this.weekForecast = weekForecast;
    }

    public Weather getDayWeather() {
        return dayWeather;
    }

    public void setDayWeather(Weather dayWeather) {
        this.dayWeather = dayWeather;
    }

    public String getJsonForecastThreeDay() {
        return jsonForecastThreeDay;
    }

    public void setJsonForecastThreeDay(String jsonForecastThreeDay) {
        this.jsonForecastThreeDay = jsonForecastThreeDay;
    }

    public String getJsonForecastWeek() {
        return jsonForecastWeek;
    }

    public void setJsonForecastWeek(String jsonForecastWeek) {
        this.jsonForecastWeek = jsonForecastWeek;
    }



}
