package ru.antonchik.fourxxi.TestWeather.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import ru.antonchik.fourxxi.TestWeather.R;
import ru.antonchik.fourxxi.TestWeather.activities.StartTest;
import ru.antonchik.fourxxi.TestWeather.adapters.CityAdapter;
import ru.antonchik.fourxxi.TestWeather.fragments.base.BaseFragment;
import ru.antonchik.fourxxi.TestWeather.models.City;
import ru.antonchik.fourxxi.TestWeather.network.OperationCallback;
import ru.antonchik.fourxxi.TestWeather.network.TestClient;
import ru.antonchik.fourxxi.TestWeather.orm.DataSource;
import ru.antonchik.fourxxi.TestWeather.owm.Weather;
import ru.antonchik.fourxxi.TestWeather.utils.TestHelper;

import java.util.ArrayList;

/**
 * Created by alekse on 07.07.14.
 */
public class SideFragment extends BaseFragment {

    private StartTest mActivity;
    private ListView cityList;
    private CityAdapter cityAdapter;
    private ArrayList<City> items;
    private View footerView;
    private Button addCityButton;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_menu, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initUI();
        setListeners();
    }

    private void initUI()
    {
        items = DataSource.getCities();
        cityList = (ListView)  getView().findViewById(R.id.cityList);
        cityAdapter =  new CityAdapter(getActivity(),items);
        footerView = getActivity().getLayoutInflater().inflate(R.layout.fragment_menu_footer,null);
        cityList.addFooterView(footerView);
        cityList.setAdapter(cityAdapter);
        addCityButton = (Button) footerView.findViewById(R.id.addCity);
    }

    private void setListeners()
    {
       cityList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
           @Override
           public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
               DataSource.setActiveCity((City) cityAdapter.getItem(i));
               ((StartTest) getActivity()).newCityChoose((City) cityAdapter.getItem(i),true);
               updateList();
           }
       });
       cityList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
           @Override
           public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
               City cityForDelete = (City) cityAdapter.getItem(i);
               if(cityForDelete.isActive())
               {
                  cityForDelete.setActive(false);
                  for(City city :items)
                  {
                      if(!city.getCityName().equals(cityForDelete.getCityName()))
                      {
                          city.setActive(true);
                          City.updateCity(city);
                          ((StartTest) getActivity()).newCityChoose(city,false);
                          continue;
                      }
                  }
               }
               City.deleteCity(cityForDelete);
               updateList();

               return false;
           }
       });

       addCityButton.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               showAddDialog();
           }
       });
    }

    public void addCity(final City city)
    {
        if(TestHelper.isNetworkConnected(getActivity()))
        {
            TestClient.getStatusForCity(getActivity(), city, new OperationCallback<Weather>() {
                @Override
                protected void onStart() {
                }

                @Override
                protected void onCompleted(Weather result) {
                    City.newCity(city);

                    City addedCity = DataSource.getCityByName(city.getCityName());
                    if(addedCity != null)
                    {
                    DataSource.setActiveCity(addedCity);
                    ((StartTest) getActivity()).newCityChoose(addedCity,true);
                    }
                    updateList();
                }

                @Override
                protected void onError(Exception error) {
                }
            });
        }
        else
        {
            TestHelper.showToast(getActivity(), getResources().getString(R.string.internet));
        }
    }

    public void showAddDialog()
    {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());

        // Setting Dialog Title
        alertDialog.setTitle("Добавить город");

        // Setting Dialog Message
        alertDialog.setMessage("Введите название города:");
        final EditText input = new EditText(getActivity());
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        input.setLayoutParams(lp);
        alertDialog.setView(input);



        // Setting Positive "Yes" Button
        alertDialog.setPositiveButton("Добавить",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if (input.getText().length() > 0) {
                            String cityName = input.getText().toString();
                            char first = Character.toUpperCase(cityName.charAt(0));
                            cityName = first + cityName.substring(1);
                            City newCity = new City("0",cityName,false,0.0,0.0);
                            addCity(newCity);
                            dialog.cancel();
                        }

                    }
                });
        // Setting Negative "NO" Button
        alertDialog.setNegativeButton("Отмена",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Write your code here to execute after dialog
                        dialog.cancel();
                    }
                });

        alertDialog.show();
    }

    public void updateList()
    {
        items = DataSource.getCities();
        cityAdapter.setData(items);
    }
}
