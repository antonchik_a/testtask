package ru.antonchik.fourxxi.TestWeather.fragments;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.koushikdutta.ion.Ion;
import info.hoang8f.android.segmented.SegmentedGroup;
import ru.antonchik.fourxxi.TestWeather.R;
import ru.antonchik.fourxxi.TestWeather.adapters.ForecastAdapter;
import ru.antonchik.fourxxi.TestWeather.fragments.base.BaseFragment;
import ru.antonchik.fourxxi.TestWeather.models.City;
import ru.antonchik.fourxxi.TestWeather.network.OperationCallback;
import ru.antonchik.fourxxi.TestWeather.network.TestClient;
import ru.antonchik.fourxxi.TestWeather.orm.DataSource;
import ru.antonchik.fourxxi.TestWeather.owm.Weather;
import ru.antonchik.fourxxi.TestWeather.utils.TestHelper;

import java.util.ArrayList;

/**
 * Created by alekse on 07.07.14.
 */
public class WeatherFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener,
        RadioGroup.OnCheckedChangeListener{

    private View headerView;
    private ListView forecastList;
    private TextView statusDegree;
    private LinearLayout statusLayout;
    private TextView statusDescription;
    private TextView statusWind;
    private ImageView statusIcon;
    private City current;
    private ForecastAdapter forecastAdapter;
    private SegmentedGroup segmented;
    private RadioButton three;
    private RadioButton seven;
    private ArrayList<Weather> items;
    private LinearLayout main;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_weather, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initUI();
        setData();
        setListeners();

    }

    private void initUI()
    {
        swipeLayout = (SwipeRefreshLayout) getView().findViewById(R.id.swipe_container);
        setPullToRefresh(this);
        forecastList = (ListView) getView().findViewById(R.id.forecastList);
        headerView = getActivity().getLayoutInflater().inflate(R.layout.fragment_weather_header,null);
        forecastList.addHeaderView(headerView);
        forecastAdapter = new ForecastAdapter(getActivity(),new ArrayList<Weather>());
        forecastList.setAdapter(forecastAdapter);
        forecastList.setCacheColorHint(Color.TRANSPARENT);


        statusDegree = (TextView) headerView.findViewById(R.id.statusDegree);
        statusDescription = (TextView) headerView.findViewById(R.id.statusDescription);
        statusWind = (TextView) headerView.findViewById(R.id.statusWind);
        statusIcon = (ImageView) headerView.findViewById(R.id.statusIcon);
        statusLayout = (LinearLayout) headerView.findViewById(R.id.statusLayout);
        segmented = (SegmentedGroup) headerView.findViewById(R.id.forecastSegment);
        segmented.setOnCheckedChangeListener(this);
        three = (RadioButton) headerView.findViewById(R.id.three);
        seven = (RadioButton) headerView.findViewById(R.id.seven);
    }

    public void setData()
    {
        current = mActivity.current;
        current.weatherAdjust();
        mActionBar.setTitle(current.getCityName());

        if(current.getDayWeather() != null)
        {
           setStatusWeather(current.getDayWeather());
           if(Weather.changeBackground(current.getDayWeather()))
           {
               swipeLayout.setBackgroundResource(R.drawable.bkg_blue);
               mActionBar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.action_bar_blue)));
           }
        }
        else
        {
           statusLayout.setVisibility(View.GONE);
           loadWeatherStatus(current);
        }

        if(current.getThreeDayForecast() != null)
        {
            forecastAdapter.setData(current.getThreeDayForecast());
        }
        else
            loadForecast(current,3);
    }
    public void setStatusWeather(Weather weather)
    {
        statusDegree.setText(weather.getDay());
        statusDescription.setText(weather.getStatus());
        statusWind.setText("Скорость ветра :" + weather.getWind_speed() +" м/с");
        Ion.with(statusIcon)
                .placeholder(R.drawable.weather)
                .error(R.drawable.weather)
                .load(TestClient.iconUrl+ weather.getIcon()+ TestClient.endIconUrl);

    }

    public void loadWeatherStatus(City city)
    {
        if(TestHelper.isNetworkConnected(getActivity()))
        {
            TestClient.getStatusForCity(getActivity(),city,new OperationCallback<Weather>() {
                @Override
                protected void onStart() {
                   forecastAdapter.setData(new ArrayList<Weather>());
                   swipeLayout.setRefreshing(true);
                   statusLayout.setVisibility(View.GONE);
                }

                @Override
                protected void onCompleted(Weather result) {
                    current = DataSource.getActiveCity();
                    setStatusWeather(result);
                    statusLayout.setVisibility(View.VISIBLE);
                    swipeLayout.setRefreshing(false);
                    setDesign();
                }

                @Override
                protected void onError(Exception error) {
                    statusLayout.setVisibility(View.GONE);
                    swipeLayout.setRefreshing(false);
                }
            });
        }
        else
        {
            TestHelper.showToast(getActivity(),getResources().getString(R.string.internet));
        }
    }

    public void loadForecast(City city,Integer count)
    {
        if(TestHelper.isNetworkConnected(getActivity()))
        {
            TestClient.getForecastForCity(getActivity(), current, count, new OperationCallback<ArrayList<Weather>>() {
                @Override
                protected void onStart() {
                  swipeLayout.setRefreshing(true);
                }

                @Override
                protected void onCompleted(ArrayList<Weather> result) {
                    forecastAdapter.setData(result);
                    swipeLayout.setRefreshing(false);
                }

                @Override
                protected void onError(Exception error) {
                    swipeLayout.setRefreshing(false);
                }
            });
        }
        else
        {
            TestHelper.showToast(getActivity(),getResources().getString(R.string.internet));
        }
    }

    private void setListeners()
    {

    }

    @Override
    public void onRefresh() {
        loadWeatherStatus(current);
        if(three.isChecked())
            loadForecast(current,3);
        else
            loadForecast(current,7);
    }

    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int i) {
        switch (i) {
            case R.id.three:
                 loadForecast(current,3);
                return;
            case R.id.seven:
                 loadForecast(current,7);
                return;

        }
    }

    public void setDesign()
    {
        if(Weather.changeBackground(current.getDayWeather()))
        {
            swipeLayout.setBackgroundResource(R.drawable.bkg_blue);
            mActionBar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.action_bar_blue)));
        }
        else
        {
            swipeLayout.setBackgroundResource(R.drawable.bkg_orange);
            mActionBar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.action_bar_yellow)));
        }
    }
}
