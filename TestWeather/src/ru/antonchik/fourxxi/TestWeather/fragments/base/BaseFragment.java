package ru.antonchik.fourxxi.TestWeather.fragments.base;

import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.widget.Toast;
import ru.antonchik.fourxxi.TestWeather.R;
import ru.antonchik.fourxxi.TestWeather.activities.StartTest;

/**
 * Created by alekse on 07.07.14.
 */
public class BaseFragment extends Fragment {

    public StartTest mActivity;
    public ActionBar mActionBar;
    public SwipeRefreshLayout swipeLayout;
    public static final int BASE = 0;
    public static final int WEATHER = BASE + 1;



    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initActionBar();
    }

    public void initActionBar() {
        mActivity = (StartTest) getActivity();
        mActionBar = mActivity.getSupportActionBar();
        mActionBar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.action_bar_yellow)));
    }

    public void showToast(String text) {
        Toast.makeText(getActivity(), text, Toast.LENGTH_LONG).show();
    }


    public void setPullToRefresh(SwipeRefreshLayout.OnRefreshListener listener) {
        swipeLayout.setOnRefreshListener(listener);
        swipeLayout.setColorScheme(R.color.blue_light,
                R.color.blue,
                R.color.blue_deep,
                R.color.blue_light);
    }

    public int getFragmentId() {
        return BASE;
    }
}
