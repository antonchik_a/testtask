package ru.antonchik.fourxxi.TestWeather.owm;

import org.json.JSONArray;
import org.json.JSONObject;
import ru.antonchik.fourxxi.TestWeather.models.City;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Alexey Antonchik on 08.07.14.
 */
public class Weather {


    public long dt;

    public Date date;

    public String day;

    public String min;

    public String max;

    public String status;


    public String weather;

    public String wind_speed;

    public String icon;


    public static Weather ajustStatus(String json,City city)
    {
       Weather result = new Weather();

        try {
            JSONObject jsonObject = new JSONObject(json);
            result.setDt(jsonObject.getLong("dt"));

            result.setDate(new java.util.Date((long)result.getDt()*1000));

            String degree = jsonObject.getJSONObject("main").getString("temp");
            if(degree.contains("."))
            {
                degree = degree.substring(0,degree.indexOf('.'));
            }

            result.setDay(degree + (char) 0x00B0);

            result.setWeather(jsonObject.getJSONArray("weather").getJSONObject(0).getString("main"));
            result.setStatus(jsonObject.getJSONArray("weather").getJSONObject(0).getString("description"));
            result.setIcon(jsonObject.getJSONArray("weather").getJSONObject(0).getString("icon"));
            result.setWind_speed(jsonObject.getJSONObject("wind").getString("speed"));
            city.setLat(jsonObject.getJSONObject("coord").getDouble("lat"));
            city.setLon(jsonObject.getJSONObject("coord").getDouble("lon"));
        }   catch (Exception ex)
        {

        }
        return  result;
    }

    public static boolean changeBackground(Weather weather)
    {
        if(weather.getWeather().toLowerCase().contains("cloud")||weather.getWeather().toLowerCase().contains("rain")||weather.getWeather().toLowerCase().contains("snow"))
            return true;
        else
            return false;
    }

    public static ArrayList<Weather> ajustForecast(String json)
    {
        ArrayList<Weather> result = new ArrayList<Weather>();
        try {
            JSONObject jsonObject = new JSONObject(json);
            JSONArray list =  jsonObject.getJSONArray("list");
            for(int i = 0;i < list.length(); i++)
            {
                JSONObject current = list.getJSONObject(i);
                Weather weather = new Weather();

                weather.setDt(current.getLong("dt"));

                weather.setDate(new java.util.Date((long)weather.getDt()*1000));

                String degree = current.getJSONObject("temp").getString("day");
                if(degree.contains("."))
                {
                    degree = degree.substring(0,degree.indexOf('.'));
                }

                weather.setDay(degree + (char) 0x00B0);

                weather.setWeather(current.getJSONArray("weather").getJSONObject(0).getString("main"));
                weather.setStatus(current.getJSONArray("weather").getJSONObject(0).getString("description"));
                weather.setIcon(current.getJSONArray("weather").getJSONObject(0).getString("icon"));
                weather.setWind_speed(current.getString("speed"));
                result.add(weather);
            }

        }   catch (Exception ex)
        {

        }
        return  result;
    }


    public long getDt() {
        return dt;
    }

    public void setDt(long dt) {
        this.dt = dt;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getWind_speed() {
        return wind_speed;
    }

    public void setWind_speed(String wind_speed) {
        this.wind_speed = wind_speed;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getWeather() {
        return weather;
    }

    public void setWeather(String weather) {
        this.weather = weather;
    }


}
