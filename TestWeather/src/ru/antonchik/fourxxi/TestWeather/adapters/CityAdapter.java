package ru.antonchik.fourxxi.TestWeather.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import ru.antonchik.fourxxi.TestWeather.items.CityItem;
import ru.antonchik.fourxxi.TestWeather.models.City;

import java.util.ArrayList;

/**
 * Created by Alexey Antonchik on 07.07.14.
 */
public class CityAdapter extends BaseAdapter {

    private Context _context;
    private ArrayList<City> items;

    public CityAdapter(Context _context,ArrayList<City> items)
    {
        this._context = _context;
        this.items = items;
    }

    public void setData(ArrayList<City> newData)
    {
        this.items = newData;
        notifyDataSetChanged();
        notifyDataSetInvalidated();
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int i) {
        return items.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        City current = (City) getItem(i);
        CityItem cell = (CityItem) view;
        if (cell != null)
            cell.updateItemContent(current);
        else
            cell = new CityItem(_context, current);

        return cell;
    }
}
