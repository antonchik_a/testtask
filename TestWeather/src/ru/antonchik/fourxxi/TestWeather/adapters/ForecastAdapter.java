package ru.antonchik.fourxxi.TestWeather.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import ru.antonchik.fourxxi.TestWeather.items.ForecastItem;
import ru.antonchik.fourxxi.TestWeather.owm.Weather;

import java.util.ArrayList;

/**
 * Created by alex on 10.07.14.
 */
public class ForecastAdapter extends BaseAdapter {
    private Context _context;
    private ArrayList<Weather> items;

    public ForecastAdapter(Context _context,ArrayList<Weather> items)
    {
        this._context = _context;
        this.items = items;
    }


    public void setData(ArrayList<Weather> newData)
    {
        this.items = newData;
        notifyDataSetChanged();
        notifyDataSetInvalidated();
    }
    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int i) {
        return items.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        Weather current = (Weather) getItem(i);
        ForecastItem cell = (ForecastItem) view;
        if (cell != null)
            cell.updateItemContent(current);
        else
            cell = new ForecastItem(_context, current);

        return cell;
    }
}
