package ru.antonchik.fourxxi.TestWeather.activities;

import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;
import android.view.View;
import ru.antonchik.fourxxi.TestWeather.R;
import ru.antonchik.fourxxi.TestWeather.fragments.SideFragment;
import ru.antonchik.fourxxi.TestWeather.fragments.WeatherFragment;
import ru.antonchik.fourxxi.TestWeather.fragments.base.BaseFragment;
import ru.antonchik.fourxxi.TestWeather.models.City;
import ru.antonchik.fourxxi.TestWeather.orm.DataSource;

public class StartTest extends ActionBarActivity {


    private DrawerLayout mDrawerLayout;
    android.support.v4.app.Fragment fragment;
    private ActionBarDrawerToggle mDrawerToggle;
    private SideFragment mDrawerMenu;
    private FragmentManager fragmentManager;
    public City current;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.Theme_AppCompat_Light_DarkActionBar);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        initUI();
        setDrawer();
        checkData();
        showFragment(BaseFragment.WEATHER,true);
    }

    public void initUI() {
        fragmentManager = getSupportFragmentManager();
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        mDrawerMenu = (SideFragment) fragmentManager.findFragmentById(R.id.sideMenu);
    }

    private void setDrawer() {

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setLogo(new ColorDrawable(getResources().getColor(android.R.color.transparent)));
        mDrawerToggle = new ActionBarDrawerToggle(
                this,
                mDrawerLayout,
                R.drawable.ic_drawer,
                R.string.drawer_open,
                R.string.drawer_close
        ) {

            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                supportInvalidateOptionsMenu();
            }

            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                supportInvalidateOptionsMenu();
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }

    public void checkData()
    {
        current =DataSource.getActiveCity();
        if(DataSource.getActiveCity() == null)
        {
            City.createInitialData();
            current =DataSource.getActiveCity();
        }
    }


    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        if (mDrawerToggle != null)
            mDrawerToggle.syncState();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Pass the event to ActionBarDrawerToggle, if it returns
        // true, then it has handled the app icon touch event
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    public void newCityChoose(City city,boolean close)
    {
        current = city;
        showFragment(BaseFragment.WEATHER,close);
    }

    private void showFragment(Integer fragmentID,boolean close) {
        switch (fragmentID) {
            case BaseFragment.WEATHER:
                    fragment = new WeatherFragment();
                break;
        }
        if (fragment != null) {
            android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.contentLayout, fragment).commitAllowingStateLoss();
        }

        if (mDrawerLayout != null && close)
            mDrawerLayout.closeDrawer(mDrawerMenu.getView());
    }
}
