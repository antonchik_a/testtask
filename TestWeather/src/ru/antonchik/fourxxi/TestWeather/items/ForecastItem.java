package ru.antonchik.fourxxi.TestWeather.items;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.koushikdutta.ion.Ion;
import ru.antonchik.fourxxi.TestWeather.R;
import ru.antonchik.fourxxi.TestWeather.network.TestClient;
import ru.antonchik.fourxxi.TestWeather.owm.Weather;
import ru.antonchik.fourxxi.TestWeather.utils.DateHelper;

/**
 * Created by alex on 11.07.14.
 */
public class ForecastItem extends LinearLayout
{


    private TextView itemDate;
    private TextView itemWind;
    private TextView itemDegree;
    private TextView itemStatus;
    private ImageView itemIcon;


    private TextView itemCurrentDegree;
    private LayoutInflater inflater;
    private Weather weather;

    public ForecastItem(Context context, Weather weather) {
        super(context);
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.forecast_cell, this, true);
        initUI();
        updateItemContent(weather);
    }

    private void initUI() {
        itemDate = (TextView) findViewById(R.id.forecastDate);
        itemWind = (TextView) findViewById(R.id.forecastWind);
        itemDegree = (TextView) findViewById(R.id.forecsatDegree);
        itemStatus = (TextView) findViewById(R.id.forecastStatus);
        itemIcon = (ImageView) findViewById(R.id.forecastIcon);
    }

    public void updateItemContent(Weather weather) {
        this.weather = weather;
        itemDegree.setText(weather.getDay());
        itemWind.setText("Скорость ветра :" + weather.getWind_speed() +" м/с");
        itemStatus.setText(weather.getStatus());
        itemDate.setText(DateHelper.getDateMonthString(weather.getDate()));
        Ion.with(itemIcon)
                .placeholder(R.drawable.weather)
                .error(R.drawable.weather)
                .load(TestClient.iconUrl+ weather.getIcon()+ TestClient.endIconUrl);


    }
}
