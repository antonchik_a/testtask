package ru.antonchik.fourxxi.TestWeather.items;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;
import ru.antonchik.fourxxi.TestWeather.R;
import ru.antonchik.fourxxi.TestWeather.models.City;

/**
 * Created by Alexey Antonchik on 08.07.14.
 */
public class CityItem extends LinearLayout {


    private TextView itemText;


    private TextView itemCurrentDegree;
    private LayoutInflater inflater;
    private City city;

    public CityItem(Context context, City city) {
        super(context);
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.city_cell, this, true);
        initUI();
        updateItemContent(city);
    }

    private void initUI() {
        itemText = (TextView) findViewById(R.id.cityName);
        itemCurrentDegree = (TextView) findViewById(R.id.cityDegree);
    }

    public void updateItemContent(City city) {
        this.city = city;
        this.city.weatherAdjust();
        itemText.setText(city.getCityName());
        if (city.getDayWeather() != null) {
            itemCurrentDegree.setText(city.getDayWeather().getDay());
            itemCurrentDegree.setVisibility(VISIBLE);
        } else {
            itemCurrentDegree.setText("");
            itemCurrentDegree.setVisibility(GONE);
        }

        if(city.isActive())
        {
            itemText.setTypeface(null, Typeface.BOLD);
        }
         else
        {
            itemText.setTypeface(null, Typeface.NORMAL);
        }

    }


    public TextView getItemText() {
        return itemText;
    }

    public void setItemText(TextView itemText) {
        this.itemText = itemText;
    }


    public TextView getItemCurrentDegree() {
        return itemCurrentDegree;
    }

    public void setItemCurrentDegree(TextView itemCurrentDegree) {
        this.itemCurrentDegree = itemCurrentDegree;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }
}
