package ru.antonchik.fourxxi.TestWeather.network;

import android.os.Handler;
import android.os.Looper;


/**
 * Created with IntelliJ IDEA.
 * User: apple2
 * Date: 16.04.13
 * Time: 11:28
 * To change this template use File | Settings | File Templates.
 */
public abstract class OperationCallbackBase implements Runnable {

    public enum DispatchType {CurrentThread, MainThread, NewThread}

    private static Handler mainHandler;

    protected void DispatchToNewThread() {
        Thread thread = new Thread(this);
        thread.start();
    }

    protected void DispatchToMainThread() {
        if (mainHandler == null) {
            mainHandler = new Handler(Looper.getMainLooper());
        }

        mainHandler.post(this);
    }
}
