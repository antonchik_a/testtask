package ru.antonchik.fourxxi.TestWeather.network;

import android.content.Context;
import android.util.Log;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import ru.antonchik.fourxxi.TestWeather.models.City;
import ru.antonchik.fourxxi.TestWeather.owm.Weather;

import java.util.ArrayList;

/**
 * Created by alex on 10.07.14.
 */
public class TestClient {


    public static String owmserver = "http://api.openweathermap.org/data/2.5";

    public static String forecast = "/forecast/daily";

    public static String status = "/weather";

    //не разобрался с параметрами так как торопился, жду ответа разработчика библиотеки
    private static String parameters = "?q=%s&mode=json&units=metric&lang=ru&APPID=2d7aa2078bab2d63d19fbb083915e077";

    private static String forecast_parameters = "?q=%s&mode=json&units=metric&lang=ru&cnt=%c&APPID=2d7aa2078bab2d63d19fbb083915e077";

    public static String iconUrl = "http://openweathermap.org/img/w/";

    public static String endIconUrl = ".png";


    public static void getStatusForCity(Context _context, final City city, final OperationCallback<Weather> callback) {

        callback.notifyStart();
        String url = owmserver + status + parameters.replace("%s", city.getCityName() + ",ru");

        Ion.with(_context)
                .load(url)
                .setLogging("MyLogs", Log.DEBUG)
                        //.addMultipartParts()
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        if (result != null) {
                            Integer responseCode = result.get("cod").getAsInt();
                            if (responseCode == 200) {
                                city.setJsonStatus(result.toString());
                                city.weatherAdjust();
                                city.updateCity(city);
                                callback.notifyCompleted(city.getDayWeather());
                            } else {
                                callback.notifyError(new Exception(responseCode.toString()));
                            }

                        } else
                            callback.notifyError(e);
                    }
                });
    }

    public static void getForecastForCity(Context _context, final City city, final int dayCount, final OperationCallback<ArrayList<Weather>> callback) {

        callback.notifyStart();
        String url = owmserver + forecast + forecast_parameters.replace("%s", city.getCityName() + ",ru").replace("%c", String.valueOf(dayCount));
        Ion.with(_context)
                .load(url)
                .setLogging("MyLogs", Log.DEBUG)
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        if (result != null) {
                            Integer responseCode = result.get("cod").getAsInt();
                            if (responseCode == 200) {
                                if (dayCount == 3)
                                    city.setJsonForecastThreeDay(result.toString());
                                else
                                    city.setJsonForecastWeek(result.toString());
                                city.weatherAdjust();
                                city.updateCity(city);
                                callback.notifyCompleted(dayCount == 3 ? city.getThreeDayForecast() : city.getWeekForecast());
                            } else {
                                callback.notifyError(new Exception(responseCode.toString()));
                            }

                        } else
                            callback.notifyError(e);
                    }
                });
    }
}
