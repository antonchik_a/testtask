package ru.antonchik.fourxxi.TestWeather.network;

import android.util.Log;

/**
 * Created with IntelliJ IDEA.
 * User: apple2
 * Date: 16.04.13
 * Time: 11:28
 * To change this template use File | Settings | File Templates.
 */
public abstract class OperationCallback<T> extends OperationCallbackBase {
    private DispatchType dispatchType;
    private T result;
    private Exception error;


    public OperationCallback() {
        Log.e("OPERATIONCALLBACK", "Constructor");
        dispatchType = DispatchType.CurrentThread;
    }

    public OperationCallback(DispatchType dispatchType) {
        this.dispatchType = dispatchType;
    }

    protected abstract void onStart();

    protected abstract void onCompleted(T result);

    protected abstract void onError(Exception error);

    public void notifyStart() {
        onStart();
    }

    public void notifyCompleted(T result) {
        try {


        Log.e("OPERATIONCALLBACK", "notifyCompletesd");
        this.result = result;

        if (dispatchType == DispatchType.MainThread) {
            DispatchToMainThread();
        } else if (dispatchType == DispatchType.NewThread) {
            DispatchToNewThread();
        } else {
            Log.e("OPERATIONCALLBACK", "start onCompleted");
            onCompleted(result);
        }
        }
        catch (Exception ex)
        {}
    }

    public void notifyError(Exception error) {
        try {
            this.error = error;

            if (dispatchType == DispatchType.MainThread) {
                DispatchToMainThread();
            } else if (dispatchType == DispatchType.NewThread) {
                DispatchToNewThread();
            } else {
                onError(error);
            }
        }
        catch (Exception ex)
        {}
    }


    public void run() {
        if (this.error != null) {
            onError(this.error);
        } else {
            onCompleted(this.result);
        }
    }
}