package ru.antonchik.fourxxi.TestWeather.app;

import android.app.Application;
import ru.antonchik.fourxxi.TestWeather.orm.DBManager;

/**
 * Created by alex on 10.07.14.
 */
public class TespApp extends Application {

    public void onCreate() {
        super.onCreate();
        DBManager.getInstance().init(getApplicationContext());
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        DBManager.getInstance().release();
    }
}
